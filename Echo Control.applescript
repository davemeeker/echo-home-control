(*

Developer assumes no liability and is not responsible for any access issues, disabled accounts, misuse or damage caused by this program.

File: Echo Control.applescript

Abstract: This script is based on the iTunes Remote Control script which demonstrates the AppleScript "Message Received" handler for Messages. It will parse incoming messages and control Indigo in response. It is further derived from the Indigo Control script which does something similar for Messages and Siri.

Version: .1

This is a sample file which must be edited to add the appropriate paths, email addresses and Indigo actions and variables.
*)

set server_app to "Path:to:Indigoserver:IndigoServer.app"
using terms from application "IndigoServer"
	set theMessage to first paragraph of getIndigoVariable("EchoCommand")
	if theMessage is not "" then
		--	log_messages("Command received: " & theMessage)
		setIndigoVariable("EchoCommandProcessed", theMessage)
		setIndigoVariable("EchoCommand", "")
		processMessage(theMessage)
	end if
	
	on processMessage(theMessage)
		try
			set thePIN to " 123"
			log_messages("Request received from Echo to " & trim_line(theMessage, thePIN, 1))
			
			set theResponse to runIndigoRemoteControl(theMessage, thePIN)
			
		on error
			log_messages("Error receiving message " & theMessage)
			
		end try
		
	end processMessage
	
	on replace_chars(this_text, search_string, replacement_string)
		set AppleScript's text item delimiters to the search_string
		set the item_list to every text item of this_text
		set AppleScript's text item delimiters to the replacement_string
		set this_text to the item_list as string
		set AppleScript's text item delimiters to ""
		return this_text
	end replace_chars
	
	on CapitalizeFirstLetter_ofEveryWord(InputString)
		set TheString to do shell script "echo " & InputString & " | tr '[A-Z]' '[a-z]'"
		set wordsofTheString to words of TheString as list
		set TotalCount to count of wordsofTheString
		set theCount to 1
		repeat until theCount is greater than TotalCount
			set theWord to item theCount of wordsofTheString
			set theChars to characters of theWord as list
			set Capital to item 1 of theChars
			set item 1 of theChars to do shell script "echo " & Capital & " | tr '[a-z]' '[A-Z]'"
			if theCount is less than TotalCount then
				set theWord to (theChars as string) & " "
			else
				set theWord to (theChars as string)
			end if
			set item theCount of wordsofTheString to theWord
			set theCount to theCount + 1
		end repeat
		set TheString to wordsofTheString as string
		return TheString
	end CapitalizeFirstLetter_ofEveryWord
	
	on trim_line(this_text, trim_chars, trim_indicator)
		-- 0 = beginning, 1 = end, 2 = both
		set x to the length of the trim_chars
		-- TRIM BEGINNING
		if the trim_indicator is in {0, 2} then
			repeat while this_text begins with the trim_chars
				try
					set this_text to characters (x + 1) thru -1 of this_text as string
				on error
					-- the text contains nothing but the trim characters
					return ""
				end try
			end repeat
		end if
		-- TRIM ENDING
		if the trim_indicator is in {1, 2} then
			repeat while this_text ends with the trim_chars
				try
					set this_text to characters 1 thru -(x + 1) of this_text as string
				on error
					-- the text contains nothing but the trim characters
					return ""
				end try
			end repeat
		end if
		return this_text
	end trim_line
	
	on log_messages(log_text)
		try
			tell application "IndigoServer"
				log log_text as string using type "Echo"
			end tell
		on error
			return "error logging message at " & (current date)
		end try
	end log_messages
	
	on getIndigoVariable(IndigoVariable)
		tell application "IndigoServer"
			set theVariableValue to get value of variable IndigoVariable
		end tell
		return theVariableValue
	end getIndigoVariable
	
	on setIndigoVariable(IndigoVariable, VariableValue)
		tell application "IndigoServer"
			set value of variable IndigoVariable to VariableValue
			set theVariableValue to get value of variable IndigoVariable
		end tell
		return IndigoVariable & " set to " & theVariableValue
	end setIndigoVariable
	
	on executeIndigoAction(IndigoAction)
		tell application "IndigoServer"
			execute group IndigoAction
		end tell
		return "Action " & IndigoAction & " Executed"
	end executeIndigoAction
	
	on turnonIndigoDevice(IndigoDevice)
		try
			tell application "IndigoServer"
				turn on IndigoDevice
			end tell
			return IndigoDevice & " was told to turn on"
			
		on error
			return "error turning on " & IndigoDevice
		end try
	end turnonIndigoDevice
	
	on turnoffIndigoDevice(IndigoDevice)
		try
			tell application "IndigoServer"
				turn off IndigoDevice
			end tell
			return IndigoDevice & " was told to turn off"
		on error
			return "error turning off " & IndigoDevice
		end try
	end turnoffIndigoDevice
	
	on dimIndigoDevice(IndigoDevice)
		try
			tell application "IndigoServer"
				dim IndigoDevice to 30
			end tell
			return IndigoDevice & " was told to dim to 30"
		on error
			return "error dimming " & IndigoDevice
		end try
	end dimIndigoDevice
	
	on brightenIndigoDevice(IndigoDevice)
		try
			tell application "IndigoServer"
				brighten IndigoDevice to 100
			end tell
			return IndigoDevice & " was told to brighten to 100"
		on error
			return "error brightening " & IndigoDevice
		end try
	end brightenIndigoDevice
	
	on sendIndigoEmail(theName, theMessage)
		try
			set theAddress to ""
			if theName is "pick_a_name" or theName is "pick_an_alternate_name" then
				set theAddress to "pick_an_email_address"
			else if theName is "pick_another_name" or theName is "pick_another_alternate_name" then
				set theAddress to "pick_another_email_address"
			end if
			if theAddress is not "" then
				set theMessage to trim_line(theMessage, "to ", 0)
				set theMessage to trim_line(theMessage, "that ", 0)
				setIndigoVariable("EmailAddress", theAddress)
				setIndigoVariable("EmailContents", theMessage)
				tell application "IndigoServer"
					execute group "Send Email"
					--need to enter a "Send Email" action script in Indigo" such as
					--tell application "IndigoServer"
					--	set emailAddress to the value of variable "EmailAddress"
					--	set emailBody to the value of variable "EmailContents"
					--	set emailSubject to "Message from my Echo"
					--	send email to emailAddress with subject emailSubject with body emailBody
					--end tell
				end tell
				return "Indigo sent email to " & theName
			else
				return "unknown email name " & theName
			end if
		on error
			return "error sending email to " & theName
		end try
	end sendIndigoEmail
	
	-- handler to respond to all incoming messages.
	on runIndigoRemoteControl(theMessage, PIN)
		try
			-- use default "unknown" command, just in case.
			set theResponse to "Unknown command " & theMessage
			if theMessage starts with "email" then
				--Email is triggered by "Alexa, Todo list, Email Name to blah blah"
				set theMessage to trim_line(theMessage, "email ", 0)
				set theMessage to trim_line(theMessage, "to ", 0)
				set theName to the first word of theMessage
				set theMessage to trim_line(theMessage, the first word of theMessage, 0)
				set theMessage to trim_line(theMessage, ".", 0)
				set theMessage to trim_line(theMessage, " ", 0)
				set theResponse to sendIndigoEmail(theName, theMessage)
			else
				
				set theMessage to trim_line(theMessage, "to ", 0)
				set theMessage to replace_chars(theMessage, "the ", "")
				
				if theMessage contains "status" then
					set alarmTriggerStatus to getIndigoVariable("AlarmTriggered")
					if alarmTriggerStatus is "true" then
						set alarmCause to getIndigoVariable("AlarmCause")
						set theResponse to "Alarm triggered by " & alarmCause
					else
						set theResponse to "No Alarm"
					end if
					
				else if theMessage contains "Set test to true" then
					
					set theResponse to setIndigoVariable("test", "true")
					
				else if theMessage contains "Set test to false" then
					
					set theResponse to setIndigoVariable("test", "false")
					
				else if theMessage contains "Disable Echo" then
					
					set theResponse to setIndigoVariable("EchoEnable", "false")
					
				else if theMessage contains "raise heat" or theMessage contains "lower AC" or theMessage contains "raise temperature" then
					
					set theResponse to executeIndigoAction("Thermostat Higher")
					
				else if theMessage contains "lower heat" or theMessage contains "raise AC" or theMessage contains "lower temperature" then
					
					set theResponse to executeIndigoAction("Thermostat Lower")
					
				else if theMessage starts with "turn on" then
					
					set targetDevice to CapitalizeFirstLetter_ofEveryWord(trim_line(theMessage, "turn on ", 0))
					
					set theResponse to turnonIndigoDevice(targetDevice)
					
				else if theMessage starts with "turn off" then
					
					set targetDevice to CapitalizeFirstLetter_ofEveryWord(trim_line(theMessage, "turn off ", 0))
					
					set theResponse to turnoffIndigoDevice(targetDevice)
					
				else if theMessage starts with "dim" then
					
					set targetDevice to CapitalizeFirstLetter_ofEveryWord(trim_line(theMessage, "dim ", 0))
					
					set theResponse to dimIndigoDevice(targetDevice)
					
				else if theMessage starts with "brighten" then
					
					set targetDevice to CapitalizeFirstLetter_ofEveryWord(trim_line(theMessage, "brighten ", 0))
					
					set theResponse to brightenIndigoDevice(targetDevice)
					
				else if theMessage starts with "execute" then
					
					set targetAction to trim_line(theMessage, "execute ", 0)
					set targetAction to trim_line(targetAction, "action ", 0)
					set targetAction to trim_line(targetAction, "group ", 0)
					set targetAction to CapitalizeFirstLetter_ofEveryWord(targetAction)
					
					set theResponse to executeIndigoAction(targetAction)
					
				else if theMessage is "help" then
					
					-- display available commands on "help"
					set theResponse to "Available commands: status, Turn on or off device, raise or lower temperature, arm alarm, execute action"
					
				end if
			end if
			
			log_messages(theResponse)
			return theResponse
		on error
			log_messages("Error processing message " & theMessage)
			return "Error processing message " & theMessage
			
		end try
		
	end runIndigoRemoteControl
	
	
end using terms from
