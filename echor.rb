a#Developer assumes no liability and is not responsible for any access issues, disabled accounts, misuse or damage caused by this program

require 'rubygems'
require 'json'
require 'open-uri'
require "mechanize" #sudo gem install rails, need xcode command line tools and accept license agreement, sudo gem install mechanize, 
require 'net/http'
require 'csv'

class Echo
	def grab
		@agent = Mechanize.new do |agent|
			agent.user_agent_alias = 'Mac Safari'
			agent.follow_meta_refresh = true
			agent.redirect_ok = true

		end

		begin 
			@agent.cookie_jar.load "cookies.yml"
			use_cached_login = true
		rescue
			use_cached_login = false
		end

		if !use_cached_login
			login_to_amazon("you@domain.com", "password!") #replace with actual username and password
		else
			#puts "using cached login from cookie"

			begin
				values = grab_tasks_from_amazon("TASK", false, false)
			rescue
				#puts "Cookie no longer working"
				login_to_amazon("you@domain.com", "password!") #replace with actual username and password
				values = grab_tasks_from_amazon("TASK", false, false)
			end
			puts values
			#not using .csv file anymore
			#CSV.open("/Users/homeserver/scripts/values.csv", "w") do |csv|
			  #csv << values
			#end
		end
	end
end

def grab_tasks_from_amazon(type, isDeleted, isComplete)
	data_url = "https://pitangui.amazon.com/api/todos?startTime=&endTime=&completed=&type=TASK&size=100&offset=-1&_=1418782198816"
	data = @agent.get(data_url)
	json_response = data.body

	result = JSON.parse(json_response)
	values = Array.new
	i = 0
	for item in result['values']
		if item['type'] == type && item['deleted'] == isDeleted && item["complete"] == isComplete
			item['complete'] = true
			#puts "adding #{item["text"]}"
			values[i] = item["text"]
			url = "https://pitangui.amazon.com/api/todos/#{item['itemId']}"
			c = @agent.cookie_jar.load("cookies.yml")
			complete = @agent.put url, item.to_json, {'Content-Type' => 'application/json', 'csrf'=>c['amazon.com']['/']['csrf'].value }
			i = i + 1
		end
	end
	return values
end				

def login_to_amazon(username, password)
	#puts "logging into amazon and setting new cookie"
	login_url = "https://www.amazon.com/ap/signin?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.assoc_handle=amzn_dp_project_dee&openid.return_to=https%3A%2F%2Fpitangui.amazon.com&"
	@agent.get(login_url)

	cookie = @agent.cookies
	
	form = @agent.page.forms.first
	form.email = username
	form.password = password
	dashboard = @agent.submit(form)
	#puts "saving cookie"
	@agent.cookie_jar.save_as "cookies.yml"
end

#puts "starting process and checking for valid cookie"
give_me_data = Echo.new
#puts "grabbing data"
give_me_data.grab